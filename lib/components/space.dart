import 'package:flutter/material.dart';
import 'package:d8essentials/theme/styles.dart';

class Space extends StatelessWidget {
  bool vertical;
  double space;

  Space({this.space: 6.0, this.vertical: false});

  Widget build(BuildContext context) {
    double verticalSpace = vertical ? space : 6.0;
    double horizontalSpace = vertical ? 6.0 : space;

    return Container(
      width: verticalSpace,
      height: horizontalSpace,
      // TODO Fill the build function
    );
  }
}