import 'package:flutter/material.dart';
import 'package:d8essentials/theme/styles.dart';

class FlatCard extends StatelessWidget {

  Color color;
  double borderRadius;
  double borderSize;
  Widget title;
  Widget content;

  FlatCard({
    this.color: Colors.black54,
    this.borderRadius : 8.0,
    this.borderSize : 3.0,
    this.title,
    @required this.content,
  });


  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
        border: Border.all(color: color, width: borderSize)
        ),
      child: Column(
        children: <Widget>[
          (title == null ? Container() : title),
          (title == null ? Container() : Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(vertical: borderSize/2),
              color: color,)
          ),
          content,
        ],
      ),


      // TODO Fill the build function
    );
  }
}