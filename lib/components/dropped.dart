import 'package:flutter/material.dart';

class Dropped extends StatelessWidget {
  double verticalOffset;
  double horizontalOffset;
  double blurRadius;
  Widget child;

  Dropped({
    @required this.child,
    this.blurRadius: 2.0,
    this.horizontalOffset : 0.0,
    this.verticalOffset: 2.0,
  });


  Widget build(BuildContext context) {
    return Column(
      verticalDirection: VerticalDirection.up,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          child: Column(
            verticalDirection: VerticalDirection.down,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              child,
              Container(
                  width:  MediaQuery.of(context).size.width,
                  height: 1.01,
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                      color: Colors.black54,
                        offset: Offset(0.0, -1.0),
                        blurRadius: 3.0,
                      ),
                    ],
                  ),
                ),
            ],
          ),
        ),
        Container(
            width:  MediaQuery.of(context).size.width,
            height: 1.0,
            margin: EdgeInsets.only(top:5.0),
            decoration: BoxDecoration(
              color: Colors.transparent,
              boxShadow: <BoxShadow>[
                BoxShadow(
                color: Colors.black54,
                  offset: Offset(0.0, 1.0),
                  blurRadius: 3.0,
                ),
              ],
            ),
          ),
      ],
      // TODO Fill the build function
    );
  }
}