import 'package:flutter/material.dart';
import 'package:d8essentials/theme/styles.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';


class RatingStars extends StatelessWidget {
  double rating;
  double size;
  IconData icon;

  RatingStars(this.rating, {this.size: 18.0, this.icon : Icons.star});
  Widget build(BuildContext context) {
    return SmoothStarRating(
          allowHalfRating: false,
          onRatingChanged: (v) {
            rating = v;
          },
          starCount: 5,
          rating: rating,
          size: 25.0,
          color: secondary_color,
          borderColor: secondary_dark,
          spacing:5.0
      // TODO Fill the build function
    );
  }
}