import 'package:flutter/material.dart';
import 'package:d8essentials/theme/styles.dart';

class ImageListTile extends StatelessWidget {

  Widget child;
  Color borderColor;
  Color bottomColor;
  double borderWidth;

  String img;
  Function onTap;

  ImageListTile({
    @required this.child,
    this.img,
    this.onTap,
    this.borderColor : Colors.black87,
    this.bottomColor : Colors.white70,
    this.borderWidth : 2.0,
  });


  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.all(10.0),
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: bottomColor,
          borderRadius: BorderRadius.circular(16.0),
          border: Border.all(color: borderColor, width: borderWidth),
        ),
        child: Column(
          children: <Widget>[
            Container(
              height: 150.0,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16.0),
                  image: DecorationImage(
                    image: NetworkImage(img), fit: BoxFit.cover))
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              height: 60.0,
              child: child,
            ),
          ],
        ),
        // TODO Fill the build function
      ),
    );
  }
}