import 'package:flutter/material.dart';
import 'package:d8essentials/theme/styles.dart';

class SimpleTile extends StatelessWidget {

  String img;
  String title;
  String text;

  SimpleTile( {
    this.img,
    @required this.title,
    @required this.text,
  });

  @override
  Widget build(BuildContext context) {
    String img;
    String title;
    String text;


    return Container(
      child: Row(

      ),
    );
  }
}


class SimpleTileButton extends StatelessWidget {
  String img;
  String title;
  String text;
  Function onPressed;

  SimpleTileButton({
    this.img,
    @required this.title,
    @required this.text,
    @required this.onPressed});

  @override
  Widget build(BuildContext context) {

    return Container(
      child: FlatButton(
        child: Row(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width/3,
              decoration: (img == null ?
                null: BoxDecoration(image: DecorationImage(image: NetworkImage(img), fit: BoxFit.cover))
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width/3 * 2,
              child: Column(
                children: <Widget>[
                  Text(title, style: TextStyle(fontSize: 16.0),),
                  Text(text, style: TextStyle(color: Colors.black45, fontSize: 13.0, fontWeight: FontWeight.w500),),
                ],
              ),
            ),


          ],
        ),
      ),
    );
  }
}
