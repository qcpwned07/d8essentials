import 'package:flutter/material.dart';
import 'package:d8essentials/theme/styles.dart';

class MaterialDetailPage extends StatelessWidget {
  Widget backgroundWidget;
  Widget cardWidget;
  Widget bottomWidget;
  double availableScreen;
  Color topBackgroundColor;
  Color bottomBackgroundColor;
  bool border;

  MaterialDetailPage({
      @required this.backgroundWidget,
      @required this.cardWidget,
      this.bottomWidget,
      this.topBackgroundColor: Colors.blue,
      this.bottomBackgroundColor: Colors.white,
      this.border: false,
  });

  Widget build(BuildContext context) {
    availableScreen = MediaQuery.of(context).size.height -80;

    return new Stack(
      children: <Widget>[
        // The containers in the background
        new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Container(
              height: availableScreen * .70,
              color: topBackgroundColor,
              child: backgroundWidget
            ),
            new Container(
              height: availableScreen * .30,
              color: bottomBackgroundColor,

            )
          ],
        ),
        // The card widget with top padding,
        // incase if you wanted bottom padding to work,
        // set the `alignment` of container to Alignment.bottomCenter
        new Container(
          alignment: Alignment.topCenter,
          padding: new EdgeInsets.only(
              top: availableScreen * .60,
              right: 20.0,
              left: 20.0),
          child: new Container(
            height: availableScreen * 0.38,
            width: MediaQuery.of(context).size.width,
            child: new Card(
              color: Colors.white,
              elevation: 4.0,
              child: cardWidget,
            ),
          ),
        )
      ],
    );
  }
}