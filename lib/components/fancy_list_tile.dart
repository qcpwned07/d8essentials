import 'package:d8essentials/components/space.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';

class FancyListTile extends StatelessWidget {
  Widget title;
  Widget subtitle;
  Widget leading;
  Widget trailing;
  double maxHeight;
  double maxWidth;
  double borderWidth;
  Color borderColor;
  Function onTap;
  static final Widget emptyContainer = Container();

  FancyListTile({
    @required this.title,
    this.subtitle,
    this.leading,
    this.trailing,
    this.onTap,
    this.maxHeight : 80.0,
    this.maxWidth,
    this.borderColor : Colors.black26,
    this.borderWidth : 3.0,
  });

  Widget build(BuildContext context) {
    return InkWell(
      onTap: (onTap == null ? (){} : onTap),
      child: Container(
        padding: EdgeInsets.only(
            right: MediaQuery.of(context).size.width * 0.05,
          left: 5.0,
          top: 5.0,
          bottom: 5.0,

        ),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: maxHeight,
                child: Transform.translate(
                    offset: Offset(8.0, 0.0),
                    child: (leading == null ? Container() : leading)
                ),
              ),
              Expanded(
                child: Container(
                  height: maxHeight - maxHeight * 0.2,
                  // Inserer des bordures sur les trois cote gauche du contenu
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.horizontal(right:Radius.circular(8.0)),
                    border: Border(
                      top: BorderSide(color: borderColor, width: borderWidth),
                      right: BorderSide(color: borderColor, width: borderWidth),
                      bottom: BorderSide(color: borderColor, width: borderWidth),
                      left: BorderSide(color: borderColor, width: borderWidth),
                    ),
                  ),
                  padding: EdgeInsets.all(5.0),
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            (title == null ? Container() : title),
                            Space(),
                            (subtitle == null ? Container() : subtitle),
                          ],
                        ),
                      ),
                      (trailing == null ? Container() : trailing),
                    ],
                  ),
                ),
              ),
            ],
          // TODO Fill the build function
        ),
      ),
    );
  }
}