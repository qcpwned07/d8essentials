import 'package:flutter/material.dart';
import 'package:d8essentials/theme/styles.dart';

class SimpleOverlay extends StatelessWidget {

  Widget content;
  String title;
  Widget customTitle;
  IconData icon;
  Function action;
  String actionLabel;
  bool hasPadding;

  //
  SimpleOverlay({
    @required this.content,
    this.title : "Title" ,
    this.customTitle,
    this.icon,
    this.action,
    this.actionLabel: "Send",
    this.hasPadding: true,
  });


  Widget build(BuildContext context) {
    return AlertDialog(
      titlePadding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
      contentPadding: (hasPadding ?
            EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0)
            : EdgeInsets.symmetric(horizontal: 0.0, vertical: 5.0)),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
      title: (customTitle != null ? customTitle : Row(
        children : [
          (icon != null ? Icon(icon, size: 38.0, color: primary_dark,) : Container()),
          Container(padding: EdgeInsets.symmetric(horizontal: 4.0),),
          Text(
            "${title}",
            style: TextStyle(color: primary_dark, fontWeight: FontWeight.w400, fontSize: 22.0, fontStyle: FontStyle.italic),
            textAlign: TextAlign.center,
          ),
        ],)
      ),
      content: content,
      actions: <Widget>[
        // usually buttons at the bottom of the dialog
         Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              child: new FlatButton(
                child: new Text("Close"),
                onPressed: () {
                  Navigator.of(context).pop();
                  },
                ),
            ),
            (action == null ?
              Text("") : FlatButton(
                child: Text(actionLabel, style: TextStyle(color: primary_dark),),
                onPressed: action,
              )
            ),
        ],
        ),
      ],
    );
  }
}