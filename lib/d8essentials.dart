import 'dart:async';

import 'package:flutter/services.dart';

class D8essentials {
  static const MethodChannel _channel =
      const MethodChannel('d8essentials');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
