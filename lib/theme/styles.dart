import 'package:flutter/material.dart';


final TextStyle title_style =  TextStyle( color: Colors.black54, fontSize: 30.0);
final TextStyle small_title_style =  TextStyle( color: Colors.black87, fontSize: 20.0);




final TextStyle dialog_title_style = TextStyle(color: primary_dark, fontWeight: FontWeight.w300, fontSize: 22.0, fontStyle: FontStyle.italic);
final TextStyle dialog_text_style_primary = TextStyle(color: primary_dark, fontWeight: FontWeight.w300, fontSize: 15.0, fontStyle: FontStyle.italic);
final TextStyle dialog_text_style_bold = TextStyle(color: Colors.black45, fontWeight: FontWeight.w500, fontSize: 15.0, fontStyle: FontStyle.normal);
final TextStyle dialog_text_style = TextStyle(color: Colors.black54, fontWeight: FontWeight.w300, fontSize: 15.0, fontStyle: FontStyle.italic);

final TextStyle dateStyle = TextStyle(color: Colors.black54, fontSize: 17.0, );
final TextStyle timeStyle = TextStyle(color: Colors.black54, fontSize: 17.0, fontWeight: FontWeight.w600);

// COLORS
final Color primary_accent = Colors.pink[600];
final Color primary_color = Colors.pink[800];
final Color primary_dark = Colors.pink[900];


final Color secondary_accent = Colors.amber[600];
final Color secondary_color = Colors.amber[700];
final Color secondary_dark = Colors.amber[800];


// Sizes
double title_icon_size = 40.0 ;