
# D8 Essentials
Generic components for your projects

## Generic component for a quickstart in your new projects

## Getting Started

Simply clone into your project and add the files of the widgets you want to use 
to your project.

For more informations about D8 Media, or additionnal documentation about the components
you can go to [D8 Media official's website](www.d8media.app)

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
Android and/or iOS.

For help getting started with Flutter, view our 
[online documentation](https://flutter.dev/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.
